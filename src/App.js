import './App.css';

import React from 'react';

import IntroSection from './sections/IntroSection';
import ProjectSection from './sections/ProjectSection';
import SkillSection from './sections/SkillSection';
import ContactSection from './sections/ContactSection';
import Navbar from './components/Navbar';

export default function App() {
	return (
		<main className='App'>
			<Navbar />
			<IntroSection />
			<ProjectSection />
			<SkillSection />
			<ContactSection />
		</main>
	);
}
