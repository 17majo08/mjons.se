import React from 'react';

import './Section.css';

export default function ContactSection() {
	return (
		<div id='contact' className='Section' style={ComponentStyle}>
			<h1>Questions are free</h1>
			<p className='SectionDescription'>
				If you feel like you need to ask me something about what I do, what I
				can do, what my favourite color is or questions of the like, don't
				hesitate to contact me. Fill out the form below and I'll get back to you
				as soon as I can.
			</p>
			<p className='SectionDescription' style={{ color: 'pink' }}>
				Skapa ett kontaktformulär med toasts och använd emailjs för att skicka
				meddelanden! :)
			</p>
		</div>
	);
}

const ComponentStyle = {
	//backgroundColor: 'green',
	backgroundImage: 'linear-gradient(180deg, #414141, #ffffcc)',
};
