import React from 'react';
import Particles from 'react-particles-js';

import ImageColumn from '../components/intro/ImageColumn';
import TextColumn from '../components/intro/TextColumn';

import './Section.css';

export default function IntroSection() {
	return (
		<main id='intro' className='Section' style={ComponentStyle}>
			<div style={{ position: 'absolute', zIndex: -1 }}>
				<Particles
					height='100vh'
					width='100vw'
					params={ParticlesConfiguration}
				/>
			</div>
			<div className='Container'>
				<div className='RowFlexContainer'>
					<ImageColumn style={PictureMediaStyle} />
					<TextColumn />
				</div>
			</div>
		</main>
	);
}

const ComponentStyle = {
	backgroundColor: '#414141',
	position: 'relative',
	overflow: 'hidden',
	zIndex: 1,
};

const PictureMediaStyle = {
	width: '60vw',
	minWidth: '250px',
	maxWidth: '200px',
	alignSelf: 'center',
};

const ParticlesConfiguration = {
	particles: {
		number: {
			value: 160,
			density: {
				enable: false,
			},
		},
		size: {
			value: 3,
			random: true,
			anim: {
				speed: 4,
				size_min: 0.3,
			},
		},
		line_linked: {
			enable: false,
		},
		move: {
			random: true,
			speed: 1,
			direction: 'top',
			out_mode: 'out',
		},
	},
	interactivity: {
		events: {
			onhover: {
				enable: true,
				mode: 'bubble',
			},
			onclick: {
				enable: true,
				mode: 'repulse',
			},
		},
		modes: {
			bubble: {
				distance: 250,
				duration: 2,
				size: 0,
				opacity: 0,
			},
			repulse: {
				distance: 400,
				duration: 4,
			},
		},
	},
};
