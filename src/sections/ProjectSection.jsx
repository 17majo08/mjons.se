import React from 'react';

import './Section.css';

import ProjectGrid from '../components/project/ProjectGrid';

export default function ProjectSection() {
	return (
		<div id='project' className='Section' style={ComponentStyle}>
			<h1>Projects</h1>
			<p className='SectionDescription'>
				Bits and pieces of things I've been working on over the past years. This
				includes employments as well as school- and side projects. Most of these
				projects can be found on my Gitlab.
			</p>
			<p className='SectionDescription' style={{ color: 'pink' }}>
				Fixa bilder och skapa länkar från titel till Gitlab och från
				språk/framework till samma dialog som från skill sektionen! :)
			</p>
			<ProjectGrid />
		</div>
	);
}

const ComponentStyle = {
	//backgroundColor: 'blue',
	backgroundImage: 'linear-gradient(180deg, #414141, #ffffcc)',
};
