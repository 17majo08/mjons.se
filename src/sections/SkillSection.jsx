import React from 'react';

import { SiUnity } from 'react-icons/si';
import { FiDatabase } from 'react-icons/fi';

import './Section.css';

export default function SkillSection() {
	return (
		<div id='skill' className='Section' style={ComponentStyle}>
			<h1>What I do</h1>
			<p className='SectionDescription'>
				Bits and pieces of things I've been working on over the past years. This
				includes employments as well as school- and side projects. Most of these
				projects can be found on my Gitlab.
			</p>
			<p className='SectionDescription' style={{ color: 'pink' }}>
				Gör en dialog som öppnar utifrån klick på en ikon och berätta lite om
				vad jag gör inom området och hur jag kom i kontakt med det! :)
			</p>
			<i className='devicon-react-original-wordmark SkillIcon' />
			<i className='devicon-csharp-plain SkillIcon' />
			<i className='devicon-java-plain-wordmark SkillIcon' />
			<i className='devicon-ocaml-plain-wordmark SkillIcon' />
			<i className='devicon-git-plain-wordmark SkillIcon' />
			<i className='devicon-gitlab-plain-wordmark SkillIcon' />
			<SiUnity className='SkillIcon' />
			<FiDatabase className='SkillIcon' />
		</div>
	);
}

const ComponentStyle = {
	//backgroundColor: 'orange',
	backgroundImage: 'linear-gradient(180deg, #414141, #ffffcc)',
};
