import React from 'react';
import { Media } from 'reactstrap';
import Typewriter from 'typewriter-effect';
import { AiOutlineGitlab, AiFillLinkedin, AiOutlineMail } from 'react-icons/ai';
import { SiCodewars } from 'react-icons/si';

import Logo from '../../pictures/profile_middle.png';

export default function ImageColumn() {
	return (
		<div
			className='ColumnFlexContainer'
			style={{
				justifyContent: 'center',
				alignItems: 'center',
				rowGap: '1px',
			}}>
			<Media
				className='FlexItem'
				object
				src={Logo}
				alt='Background'
				style={LogoStyle}
			/>
			<h3 className='FlexItem'>Max Jonsson</h3>
			<h4>
				<Typewriter
					options={{
						strings: ['Software developer', 'Web designer', 'Coach', 'Learner'],
						autoStart: true,
						loop: true,
					}}
				/>
			</h4>
			<div className='RowFlexContainer iconContainer'>
				<a href='https://gitlab.com/17majo08' target='_blank' rel='noreferrer'>
					<AiOutlineGitlab className='icon' />
				</a>
				<a
					href='https://se.linkedin.com/in/max-jonsson-72601318a'
					target='_blank'
					rel='noreferrer'>
					<AiFillLinkedin className='icon' />
				</a>
				<a href='mailto:maxjonsson9@gmail.com'>
					<AiOutlineMail className='icon' />
				</a>
				<a
					href='https://www.codewars.com/users/Maaxi'
					target='_blank'
					rel='noreferrer'>
					<SiCodewars className='icon' />
				</a>
			</div>
		</div>
	);
}

const LogoStyle = {
	borderRadius: '50% 25%',
	//border: '1px solid #000000',
	maxWidth: '300px',
	boxShadow: '25px 25px 50px 0 rgba(0, 0, 0, 0.8)',
	//boxShadow: '25px 25px 50px 0 white inset, -25px -25px 50px 0 white inset',
	marginBottom: '20px',
	backgroundImage: 'radial-gradient(#000 10%, #414141 70%)',
};
