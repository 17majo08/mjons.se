import React from 'react';
import { Media, Card, CardTitle } from 'reactstrap';

export default function TextColumn() {
	return (
		<Media className='ColumnFlexContainer'>
			<Card className='ListCard' body style={CardStyle}>
				<CardTitle>
					<h2>Biography</h2>
				</CardTitle>
				<p>
					I am a passionate programmer and a coach, born and raised in
					Söderhamn, Sweden. I am a full stack developer with React.js, Redux,
					Express.js, Node.js, and PostgreSQL in my tech stack. In 2020 I
					successfully completed my engineering degree with specialization in
					computer science. My goal is to always stay driven towards providing
					an amazing experience with the highest level of quality and service.
					Along with that, I also coach people on their journey to become a
					professional developer. I love learning about new technologies and how
					I can use them to build better and scalable products.
				</p>
				<p>
					I am a Master’s student in the Department of Computer Science at the
					University of Texas at Dallas graduating in May 2021.
				</p>
				<p>
					I have a Bachelor of Technology degree from the Indian Institute of
					Technology (IIT) Guwahati. Previously, I worked at Works Applications,
					Singapore as a Software Engineer, where I worked on delivering
					libraries, microservices and distributed multi-tenant architecture to
					thousands of developers inside the company who utilized them to build
					various products for our customers. Aside from work, I am also an
					open-source enthusiast and have created projects that were listed in
					GitHub trending.
				</p>
				<p>
					My interests lie in solving problems related to large-scale
					distributed software systems.
				</p>
			</Card>
			<Media
				className='RowFlexContainer'
				style={{ width: '100%', columnGap: '20px', alignItems: 'stretch' }}>
				<Card className='ListCard' body style={CardStyle}>
					<CardTitle>
						<h3>Interests</h3>
					</CardTitle>
					<ul>
						<li>Distributed Computing</li>
						<li>Software Engineering</li>
						<li>Information Security</li>
					</ul>
				</Card>
				<Card className='ListCard' body style={CardStyle}>
					<CardTitle>
						<h3>Education</h3>
					</CardTitle>
					<ul>
						<li>MS in Computer Science, 2021</li>
						<li>BTech, 2016</li>
					</ul>
				</Card>
			</Media>
		</Media>
	);
}

const CardStyle = {
	backgroundColor: '#333',
	borderColor: '#333',
	marginTop: '20px',
};
