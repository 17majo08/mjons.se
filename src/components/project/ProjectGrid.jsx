import React from 'react';
import { CardDeck } from 'reactstrap';

import ProjectCard from './ProjectCard';

export default function ProjectGrid() {
	return (
		<CardDeck className='ProjectContainer'>
			<ProjectCard
				title='Asteroids'
				subtitle='Java'
				description='This is a wider card with supporting text below as a natural lead-in
						to additional content. This content is a little bit longer.'
			/>
			<ProjectCard
				title='Sportstats'
				subtitle='C#, MSSQL Server'
				description='This is a wider card with supporting text below as a natural lead-in
						to additional content. This content is a little bit longer.'
			/>
			<ProjectCard
				title='Candy crush'
				subtitle='Unity, C#'
				description='This is a wider card with supporting text below as a natural lead-in
						to additional content. This content is a little bit longer.'
			/>
			<ProjectCard
				title='Infinic world'
				subtitle='React native, Firebase'
				description='This is a wider card with supporting text below as a natural lead-in
						to additional content. This content is a little bit longer.'
			/>
			<ProjectCard
				title='Portfolio'
				subtitle='React, Gitlab pages'
				description='This is a wider card with supporting text below as a natural lead-in
						to additional content. This content is a little bit longer.'
			/>
			<ProjectCard
				title='Graph maker'
				subtitle='OCaml'
				description='This is a wider card with supporting text below as a natural lead-in
						to additional content. This content is a little bit longer.'
			/>
		</CardDeck>
	);
}
