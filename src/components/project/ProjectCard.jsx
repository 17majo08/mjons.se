import React from 'react';
import {
	Card,
	CardImg,
	Button,
	CardTitle,
	CardText,
	CardSubtitle,
	CardBody,
} from 'reactstrap';

import PropTypes from 'prop-types';

import Image from '../../pictures/placeholder.jpg';

export default function ProjectCard(props) {
	return (
		<Card className='ProjectCard'>
			<CardImg top width='100%' src={Image} alt='Project' />
			<CardBody>
				<CardTitle tag='h5'>{props.title}</CardTitle>
				<CardSubtitle tag='h6' className='mb-2 text-muted'>
					{props.subtitle}
				</CardSubtitle>
				<CardText>{props.description}</CardText>
				<Button>View project</Button>
			</CardBody>
		</Card>
	);
}

ProjectCard.propTypes = {
	title: PropTypes.string.isRequired,
	subtitle: PropTypes.string.isRequired,
	description: PropTypes.string.isRequired,
};
