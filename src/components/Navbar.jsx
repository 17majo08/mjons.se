import React, { useState } from 'react';
import {
	Collapse,
	Navbar,
	NavbarToggler,
	NavbarBrand,
	Nav,
	NavItem,
	Media,
} from 'reactstrap';
import AnchorLink from 'react-anchor-link-smooth-scroll';

import Banner from '../pictures/banner.svg';

export default function CustomNavbar() {
	const [isOpen, setIsOpen] = useState(false);
	const toggle = () => setIsOpen(!isOpen);

	return (
		<div id='top' className='navbarContainer'>
			<Navbar dark expand='md'>
				<NavbarBrand href='/'>
					<Media
						object
						src={Banner}
						alt='Background'
						style={BannerStyle}></Media>
				</NavbarBrand>
				<NavbarToggler onClick={toggle} />
				<Collapse isOpen={isOpen} navbar>
					<Nav className='ml-auto' navbar>
						<NavItem>
							<AnchorLink href='#intro' className='nav-link'>
								About
							</AnchorLink>
						</NavItem>
						<NavItem>
							<AnchorLink href='#project' className='nav-link'>
								Projects
							</AnchorLink>
						</NavItem>
						<NavItem>
							<AnchorLink href='#skill' className='nav-link'>
								Skills
							</AnchorLink>
						</NavItem>
						<NavItem>
							<AnchorLink href='#contact' className='nav-link'>
								Contact
							</AnchorLink>
						</NavItem>
					</Nav>
				</Collapse>
			</Navbar>
		</div>
	);
}

const BannerStyle = {
	height: '60px',
};
